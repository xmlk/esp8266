#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <PubSubClient.h>

#define wifi_ssid ""
#define wifi_password ""

const char* server = "";

WiFiUDP udp;
WiFiClient espClient;
PubSubClient client(espClient);

void sendWOL(const IPAddress ip, const byte mac[]);
void macStringToBytes(const String mac, byte *bytes);


void setup() {
  Serial.begin(115200);
  printMac();
  Serial.print("");
  Serial.print("connecting to ");
  Serial.println(wifi_ssid);
  WiFi.begin(wifi_ssid, wifi_password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(5000);
    Serial.print(". ... .. ");
    Serial.print("");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  udp.begin(9);
  client.setServer(server, 1883);
  client.setCallback(callback);
}


void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    if (client.connect("espClient")) {
      Serial.println("connected");
      client.publish("wol/2","esp");
      client.subscribe("wol/1");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
  String mac = "";
  for (int i = 0; i < length; i++) {
    if((char)payload[i]=='&'){
      IPAddress target_ip;
      target_ip = WiFi.localIP();
      target_ip[3] = 255;
      Serial.println("Sending WOL");
      Serial.println(target_ip);
      byte target_mac[6];
      macStringToBytes(mac, target_mac);

      sendWOL(target_ip, target_mac);
      sendMqtt(mac);
      mac = "";
      delay(10000);
     }else{
      mac += (char)payload[i];
     }
  }
}

void sendWOL(const IPAddress ip, const byte mac[]) {
  byte preamble[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
  udp.beginPacket(ip, 9);
  udp.write(preamble, 6);
  for (uint8 i = 0; i < 16; i++) {
    udp.write(mac, 6);
  }
  udp.endPacket();
}

void sendMqtt(String msg){
  char msg_buffer[msg.length()+2];
  msg.toCharArray(msg_buffer, msg.length()+2);
  client.publish("wol/2", msg_buffer);
}


byte valFromChar(char c) {
  if(c >= 'a' && c <= 'f') return ((byte) (c - 'a') + 10) & 0x0F;
  if(c >= 'A' && c <= 'F') return ((byte) (c - 'A') + 10) & 0x0F;
  if(c >= '0' && c <= '9') return ((byte) (c - '0')) & 0x0F;
  return 0;
}

void macStringToBytes(const String mac, byte *bytes) {
  if(mac.length() >= 12) {
    for(int i = 0; i < 6; i++) {
      bytes[i] = (valFromChar(mac.charAt(i*2)) << 4) | valFromChar(mac.charAt(i*2 + 1));
    }
  } else {
    Serial.println("Incorrect MAC format.");
  }
}

void printMac(){
  String clientMac = "";
  unsigned char mac[6];
  WiFi.macAddress(mac);
  clientMac += macToStr(mac);
  Serial.println(clientMac);
}

String macToStr(const uint8_t* mac){
  String result;
  for (int i = 0; i < 6; ++i) {
    result += String(mac[i], 16);
    if (i < 5)
    result += ':';
  }
  return result;
}

