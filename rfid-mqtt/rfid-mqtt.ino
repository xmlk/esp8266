#include "MFRC522.h"
#include <ESP8266WiFi.h>
#define RST_PIN 5
#define SS_PIN 4
#define topic "topic_rfid"
#define mqtt_server "mqtt_server"
#define wifi_ssid "Kleebert"
#define wifi_password "idTBfrnJo3GhHxMtr3weVDFN"
MFRC522 rfid(SS_PIN, RST_PIN);

WiFiClient espClient;
//PubSubClient client(espClient);

void setup() {
  Serial.begin(115200);
  //setup_wifi();
  SPI.begin();
  rfid.PCD_Init();
}

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting");
  Serial.println(wifi_ssid);

  WiFi.begin(wifi_ssid, wifi_password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  read_rfid();
}

void read_rfid() {
  if ( ! rfid.PICC_IsNewCardPresent()) {
    delay(50);
    return;
  }
  if ( ! rfid.PICC_ReadCardSerial()) {
    delay(50);
    return;
  }
  
  Serial.print(F("Card UID:"));
  Serial.println(printHex(rfid.uid.uidByte, rfid.uid.size));
}

String printHex(byte *buffer, byte bufferSize) {
  String id = "";
  for (byte i = 0; i < bufferSize; i++) {
    id += buffer[i] < 0x10 ? "0" : "";
    id += String(buffer[i], HEX);
  }
  return id;
}
