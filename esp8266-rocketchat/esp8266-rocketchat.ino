#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <WiFiUDP.h>
#include <WakeOnLan.h>
#define wifi_ssid "wvss-esp"
#define wifi_password "12345678"
#define mqtt_server "35.234.147.240"
#define mqtt_sub_topic "wol/1"
#define mqtt_pub_topic "wol/2"


WiFiClient espClient;
PubSubClient client(espClient);
WiFiUDP UDP;

void sendWOL();

void setup() {
  Serial.begin(115200);
  Serial.print("connecting to ");
  Serial.println(wifi_ssid);
  WiFi.begin(wifi_ssid, wifi_password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("connecting to mqtt");
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  UDP.begin(9);
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("start mqtt connection...");
    if (client.connect("espClient")) {
      Serial.println("connected");
      client.publish(mqtt_pub_topic, "esp");
      client.subscribe(mqtt_sub_topic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      delay(5000);
    }
  }
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }

client.loop();  
}

void callback(char* topic, byte* payload, unsigned int length) {

  String mac = "";
  
  Serial.print("message arrived in topic: ");
  Serial.println(topic);
  Serial.print(" ");
  for (int i = 0; i < length; i++) {
    if((char)payload[i]=='&'){
      Serial.println("In loop:");
      Serial.println(" ");
      Serial.println(mac);
      Serial.println(" ");
      wol_send(mac);

      mac = "";
    }else {
      mac += (char)payload[i];
    }
  }
}

void mqtt_send(String topic, String message){
  Serial.print("in mqtt send: ");
  Serial.print("");
  Serial.print(message);
  const char * t = topic.c_str();
  const char * m = message.c_str();
  client.publish(t, m);
}

void wol_send(String mac_str){
  char mac_buffer[mac_str.length()+2];
  mac_str.toCharArray(mac_buffer, mac_str.length()+2);
  byte mac[6];
  parseBytes(mac_buffer, ':', mac, 6, 16);
  IPAddress computer_ip(255,255,255,255);
  WakeOnLan::sendWOL(computer_ip, UDP, mac, sizeof mac);
  mqtt_send(mqtt_pub_topic,mac_str);
  Serial.print("in wol_send:");
  Serial.print(" ");
  Serial.print(mac_str);
  delay(4000);
}

void parseBytes(const char* str, char sep, byte* bytes, int maxBytes, int base) {
  for (int i = 0; i < maxBytes; i++) {
    bytes[i] = strtoul(str, NULL, base);
    str = strchr(str, sep);
    if (str == NULL || *str == '\0') {
      break;
    }
    str++;
  }
}
